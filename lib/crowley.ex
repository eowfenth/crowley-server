defmodule Crowley do
  @moduledoc """
  Documentation for Crowley.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Crowley.hello
      :world

  """
  def hello do
    :world
  end
end
