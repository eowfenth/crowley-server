defmodule Crowley.MixProject do
  use Mix.Project

  @version "0.0.1-dev"

  def project do
    [
      app: :crowley,
      version: @version,
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package(),
      source_url: "https://github.com/eowfenth/crowley-server",
      description: """
      Personal todo list.
      """
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.18.3"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
    ]
  end

  defp package do
    [
      name: "crowley",
      maintainers: [
        "Nícolas 'eowf' Deçordi"
      ],
      licenses: [
        "MIT"
      ],
      links: %{github: "https://github.com/eowfenth/crowley-server"}
    ]
  end
end
