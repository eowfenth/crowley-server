# Crowley

[![Build Status](https://travis-ci.org/eowfenth/crowley-server.svg?branch=master)](https://travis-ci.org/eowfenth/crowley-server)

**TODO**

- [x] Add Mix

- [x] Add readme

- [x] Add license

- [x] Add it to Hex

- [ ] Add it to Travis

- [ ] Do the damn todo

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `crowley` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:crowley, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/crowley](https://hexdocs.pm/crowley).

The package can be found at: [https://hex.pm/packages/crowley](https://hex.pm/packages/crowley).
